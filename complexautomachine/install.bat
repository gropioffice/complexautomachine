﻿@echo off

echo ======================================
echo GROP 複合機自動設置バッチ
echo 更新日 2016/9/09
echo ======================================
rem pause

:RESELECTOS
rem OSのbit数確認
echo ------------------------------------------------------------------------------
echo ---設置するOSのbit数番号を選択してください。
echo ------------------------------------------------------------------------------
echo ---bit数の確認方法---
echo ---①画面の左下にある丸いWindowsボタンを押します。
echo ---②右上の「コンピューター」を右クリックします。
echo ---③プロパティを選択します。
echo ---④画面の真ん中の「システム種類」からビット数を確認してください。
echo ------------------------------------------------------------------------------
echo 1) 32bit
echo 2) 64bit
set /p bitnum="番号入力："

rem 選択が1の場合
if "%bitnum%"=="1" set bitnumber=32bit

rem 選択が2の場合
if "%bitnum%"=="2" set bitnumber=64bit

rem 選択が1も2も違う場合
if NOT "%bitnum%"=="1" if NOT "%bitnum%"=="2" goto RESELECTOS
echo ------------------------------------------------------------------------------

:RESELECTMACHINE
rem 設置する複合機のドライバー選択
echo ------------------------------------------------------------------------------
echo ----設置可能複合機リスト----
echo 1) LBP351/352 LIPSLX
echo 2) LBP6600 LIPSLX
echo 3) LBP6700 LIPSLX
echo 4) LBP6710 LIPSLX
echo 5) LBP8630/8620/8610 LIPSLX
echo 6) LBP8720/8710 LIPSLX
echo 7) LBP8730 LIPSLX
echo 8) LBP8900 LIPSLX
echo 9) LBP712C LIPSLX
echo 10) LBP7600C LIPSLX
echo 11) LBP843C/842C/841C LIPSLX
echo 12) LBP9510C LIPSLX
echo 13) LBP9520C LIPSLX
echo 14) LBP9650C LIPSLX
echo 15) LBP9660C LIPSLX
echo 16) LBP9950C/9900C LIPSLX
echo 17) MF417 LIPSLX
echo 18) MF510 Series LIPSLX
echo 19) iR-ADV 4025/4035 LIPSLX
echo 20) iR-ADV 4045 LIPSLX
echo 21) iR-ADV 4225/4235 LIPSLX
echo 22) iR-ADV 4245 LIPSLX
echo 23) iR-ADV 6055/6065 LIPSLX
echo 24) iR-ADV 6075 LIPSLX
echo 25) iR-ADV 6255/6265 LIPSLX
echo 26) iR-ADV 6275 LIPSLX
echo 27) iR-ADV 6555/6565 LIPSLX
echo 28) iR-ADV 6575 LIPSLX
echo 29) iR-ADV 8085/8095 LIPSLX
echo 30) iR-ADV 8105 LIPSLX
echo 31) iR-ADV 8205 LIPSLX
echo 32) iR-ADV 8285/8295 LIPSLX
echo 33) iR-ADV 8505 LIPSLX
echo 34) iR-ADV 8585/8595 LIPSLX
echo 35) iR-ADV C2020/2030 LIPSLX
echo 36) iR-ADV C2218 LIPSLX
echo 37) iR-ADV C2220/2230 LIPSLX
echo 38) iR-ADV C3320/3330 LIPSLX
echo 39) iR-ADV C350 LIPSLX
echo 40) iR-ADV C5030/5035 LIPSLX
echo 41) iR-ADV C5045/5051 LIPSLX
echo 42) iR-ADV C5235/5240 LIPSLX
echo 43) iR-ADV C5250/5255 LIPSLX
echo 44) iR-ADV C5535/5540 LIPSLX
echo 45) iR-ADV C5550/5560 LIPSLX
echo 46) iR-ADV C7055/7065 LIPSLX
echo 47) iR-ADV C7260/7270 LIPSLX
echo 48) iR-ADV C9065 LIPSLX
echo 49) iR-ADV C9075 LIPSLX
echo 50) iR-ADV C9270/9280 LIPSLX
echo 51) iPR C6010 LIPSLX
echo 52) iPR C650 LIPSLX
echo 53) iPR C700/800 LIPSLX
echo 54) iPR C7010VP LIPSLX
echo ----設置可能複合機リスト----
echo '
echo 設置する複合機の番号を入力してください。
set /p number="番号："
if "%number%"=="1" set printername=Canon LBP351/352 LIPSLX
if "%number%"=="2" set printername=Canon LBP6600 LIPSLX
if "%number%"=="3" set printername=Canon LBP6700 LIPSLX
if "%number%"=="4" set printername=Canon LBP6710 LIPSLX
if "%number%"=="5" set printername=Canon LBP8630/8620/8610 LIPSLX
if "%number%"=="6" set printername=Canon LBP8720/8710 LIPSLX
if "%number%"=="7" set printername=Canon LBP8730 LIPSLX
if "%number%"=="8" set printername=Canon LBP8900 LIPSLX
if "%number%"=="9" set printername=Canon LBP712C LIPSLX
if "%number%"=="10" set printername=Canon LBP7600C LIPSLX
if "%number%"=="11" set printername=Canon LBP843C/842C/841C LIPSLX
if "%number%"=="12" set printername=Canon LBP9510C LIPSLX
if "%number%"=="13" set printername=Canon LBP9520C LIPSLX
if "%number%"=="14" set printername=Canon LBP9650C LIPSLX
if "%number%"=="15" set printername=Canon LBP9660C LIPSLX
if "%number%"=="16" set printername=Canon LBP9950C/9900C LIPSLX
if "%number%"=="17" set printername=Canon MF417 LIPSLX
if "%number%"=="18" set printername=Canon MF510 Series LIPSLX
if "%number%"=="19" set printername=Canon iR-ADV 4025/4035 LIPSLX
if "%number%"=="20" set printername=Canon iR-ADV 4045 LIPSLX
if "%number%"=="21" set printername=Canon iR-ADV 4225/4235 LIPSLX
if "%number%"=="22" set printername=Canon iR-ADV 4245 LIPSLX
if "%number%"=="23" set printername=Canon iR-ADV 6055/6065 LIPSLX
if "%number%"=="24" set printername=Canon iR-ADV 6075 LIPSLX
if "%number%"=="25" set printername=Canon iR-ADV 6255/6265 LIPSLX
if "%number%"=="26" set printername=Canon iR-ADV 6275 LIPSLX
if "%number%"=="27" set printername=Canon iR-ADV 6555/6565 LIPSLX
if "%number%"=="28" set printername=Canon iR-ADV 6575 LIPSLX
if "%number%"=="29" set printername=Canon iR-ADV 8085/8095 LIPSLX
if "%number%"=="30" set printername=Canon iR-ADV 8105 LIPSLX
if "%number%"=="31" set printername=Canon iR-ADV 8205 LIPSLX
if "%number%"=="32" set printername=Canon iR-ADV 8285/8295 LIPSLX
if "%number%"=="33" set printername=Canon iR-ADV 8505 LIPSLX
if "%number%"=="34" set printername=Canon iR-ADV 8585/8595 LIPSLX
if "%number%"=="35" set printername=Canon iR-ADV C2020/2030 LIPSLX
if "%number%"=="36" set printername=Canon iR-ADV C2218 LIPSLX
if "%number%"=="37" set printername=Canon iR-ADV C2220/2230 LIPSLX
if "%number%"=="38" set printername=Canon iR-ADV C3320/3330 LIPSLX
if "%number%"=="39" set printername=Canon iR-ADV C350 LIPSLX
if "%number%"=="40" set printername=Canon iR-ADV C5030/5035 LIPSLX
if "%number%"=="41" set printername=Canon iR-ADV C5045/5051 LIPSLX
if "%number%"=="42" set printername=Canon iR-ADV C5235/5240 LIPSLX
if "%number%"=="43" set printername=Canon iR-ADV C5250/5255 LIPSLX
if "%number%"=="44" set printername=Canon iR-ADV C5535/5540 LIPSLX
if "%number%"=="45" set printername=Canon iR-ADV C5550/5560 LIPSLX
if "%number%"=="46" set printername=Canon iR-ADV C7055/7065 LIPSLX
if "%number%"=="47" set printername=Canon iR-ADV C7260/7270 LIPSLX
if "%number%"=="48" set printername=Canon iR-ADV C9065 LIPSLX
if "%number%"=="49" set printername=Canon iR-ADV C9075 LIPSLX
if "%number%"=="50" set printername=Canon iR-ADV C9270/9280 LIPSLX
if "%number%"=="51" set printername=Canon iPR C6010 LIPSLX
if "%number%"=="52" set printername=Canon iPR C650 LIPSLX
if "%number%"=="53" set printername=Canon iPR C700/800 LIPSLX
if "%number%"=="54" set printername=Canon iPR C7010VP LIPSLX
if "%number%" gtr "54" goto RESELECTMACHINE
echo ------------------------------------------------------------------------------
rem 選択したドライバーの確認
set /p yn="%printername%で間違いないでしょうか。(y/n):"
if not %yn%==y goto RESELECTMACHINE

:RENAMEOFFICE
rem Office名確認
echo ------------------------------------------------------------------------------
echo ---複合機のオフィス名を入力してください。(例：岡山オフィス→【岡山】)
set /p officename="オフィス名入力："

rem 入力したオフィス名を確認
set /p officeyn="%officename%で間違いないでしょうか。(y/n):"
if %officeyn%==n goto RENAMEOFFICE
echo ------------------------------------------------------------------------------

:REIPADDRESS
rem IPアドレスの入力
echo ------------------------------------------------------------------------------
echo 複合機のIPアドレスを入力してください。
set /p ipadress="ipアドレスを入力："
set ipip=IP_%ipadress%

rem IPアドレスの入力確認
set /p ipyn="%ipadress% で間違いないでしょうか。(y/n):"
if %ipyn%==n goto REIPADDRESS
echo ------------------------------------------------------------------------------

rem 複合機の通常使うプリンターデフォルト設定を無効にする
reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows" /v "LegacyDefaultPrinterMode" /t REG_DWORD /d "1" /f

rem ドライバー設置
if "%bitnumber%"=="32bit" (
move %bitnumber% %temp%
cd %SYSTEMROOT%\System32\Printing_Admin_Scripts\ja-JP
IF ERRORLEVEL 1 GOTO FILEERROR64
cscript prnport.vbs -a -r %ipip% -h %ipadress% -me -o raw -y public -i 1 -n 9100
IF ERRORLEVEL 1 GOTO FILEERROR
rundll32 printui.dll,PrintUIEntry /if /b "%officename%_%printername%" /f "%temp%\%bitnumber%\Driver\CNLB0J.INF" /r "%ipip%" /m "%printername%"
rd %temp%\%bitnumber% /s /q
) else (
move %bitnumber% %temp%
IF ERRORLEVEL 1 GOTO FILEERROR64
cscript C:\Windows\System32\Printing_Admin_Scripts\ja-JP\prnport.vbs -a -r %ipip% -h %ipadress% -me -o raw -y public -i 1 -n 9100
IF ERRORLEVEL 1 GOTO FILEERROR
rundll32 printui.dll,PrintUIEntry /if /b "%officename%_%printername%" /f "%temp%\%bitnumber%\Driver\CNLB0JA64.INF" /r "%ipip%" /m "%printername%"
rd %temp%\%bitnumber% /s /q
)
set /p defaultpurinteryn="通常使うプリンターに設定しますか。(y/n)"
if %defaultpurinteryn%==y rundll32 printui.dll,PrintUIEntry /y /n "%officename%_%printername%"

echo 設置が完了しました。
pause
exit 1

:FILEERROR
echo ドライバーエラーが発生しました。情報システム室にお問い合わせください。
pause
exit 1

:FILEERROR64
echo OSバージョンエラーが発生しました。OSのバージョンやビット数に誤りがないか確認してください。
pause
exit 1